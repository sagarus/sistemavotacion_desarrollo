/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.vistas;

import java.util.Random;
import ufps.util.colecciones_seed.Pila;

/**
 *
 * @author madar
 */
public class TesPilaAleatoria {
    
    public static void main(String[] args) {
        Pila<Integer> p=new Pila();
        
        p.apilar(1);
        p.apilar(100);
        p.apilar(1000);
        p.apilar(2000);
        
        int tam=p.getTamanio();
        System.out.println("Tamaño de la pila es:"+tam);
        
        Random x=new Random();
//        int i=10;
//        while(i-->0)
//            System.out.println(x.nextInt(tam));
        
        int posRandom=x.nextInt(tam);
        int datoPila=0;
        Pila<Integer> aux=new Pila();
        while(posRandom>=0)
        {
            datoPila=p.desapilar();
            aux.apilar(datoPila);
            posRandom--;
        }
        //vaciar:
        while(!aux.esVacia())
            p.apilar(aux.desapilar());
        
        System.out.println("-------------- Dato random:"+datoPila+"-------------------");
        System.out.println("Imprimiendo la pila:");
        while(!p.esVacia())
                System.out.println(p.desapilar()+"\t");
        
        
    }
    
}
