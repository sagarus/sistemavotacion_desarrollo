package Entidad;

import java.util.Random;
import ufps.util.colecciones_seed.ListaCD;

public class Municipio {

    private int id_municipio;

    private String nombre;
    
    private ListaCD<CentroVotacion> centros=new ListaCD();

    public Municipio() {
    }

    public Municipio(int id_municipio, String nombre) {
        this.id_municipio = id_municipio;
        this.nombre = nombre;
    }

    public int getId_municipio() {
        return id_municipio;
    }

    public void setId_municipio(int id_municipio) {
        this.id_municipio = id_municipio;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public ListaCD<CentroVotacion> getCentros() {
        return centros;
    }

    public void setCentros(ListaCD<CentroVotacion> centros) {
        this.centros = centros;
    }

    @Override
    public String toString() {
        return "\n Municipio{" + "id_municipio=" + id_municipio + ", nombre=" + nombre + 
                "Mis centros son:"+this.getListadoCentros()+'}';
    }
    
    public String getListadoCentros()
    {
    String msg="";
    for(CentroVotacion dato:this.centros)
        msg+=dato.toString()+"\n";
    
    return msg;
    }
    
    public CentroVotacion getCentroAleatorio()
    {
        
      Random rnd = new Random();
	
        int x = (int)(rnd.nextDouble() * centros.getTamanio()-1)+1;
        for(CentroVotacion rta:this.centros){
            if(rta.getId_centro()==x)
                return rta;
            }
        
        return null;
    }
}
